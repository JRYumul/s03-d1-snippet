// Username is hard-coded here since Gitlab has no back end
let uname = "demo@mail.com"

$("#checkemail").keyup(function(){
    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($(this).val())){
        if($(this).val() == uname){
            $("#emailok").removeClass("text-black-50 text-success")
            $("#emailok").addClass("text-danger")
            $("#emailok").html("Email is unavailable ")
            $("#emailok").nextUntil("p").remove()
            $("#emailok").append('<i class="fas fa-times"></i>')
        }else{
            $("#emailok").removeClass("text-black-50 text-danger")
            $("#emailok").addClass("text-success")
            $("#emailok").html("Email is available ")
            $("#emailok").nextUntil("p").remove()
            $("#emailok").append('<i class="fas fa-check"></i>')
        }
    }else{
        $("#emailok").removeClass("*")
        $("#emailok").addClass("text-black-50")
        $("#emailok").html("Email must be available ")
        $("#emailok").nextUntil("p").remove()
    }
});

$("#pw1, #pw2").keyup(function(){
    if($("#pw1").val() == ""){
        $("#passok1").removeClass("*")
        $("#passok1").addClass("text-black-50")
        $("#passok1").html("Password must contain at least 8 characters ")
        $("#passok1").nextUntil("p").remove()
        $("#passok2").removeClass("*")
        $("#passok2").addClass("text-black-50")
        $("#passok2").html("Password cannot contain spaces ")
        $("#passok2").nextUntil("p").remove()
        $("#passok3").removeClass("*")
        $("#passok3").addClass("text-black-50")
        $("#passok3").html("Passwords must match ")
        $("#passok3").nextUntil("p").remove()
    }else{
        if($("#pw1").val().length < 8){
            $("#passok1").removeClass("text-black-50 text-success")
            $("#passok1").addClass("text-danger")
            $("#passok1").html("Password does not have at least 8 characters ")
            $("#passok1").nextUntil("p").remove()
            $("#passok1").append('<i class="fas fa-times"></i>')
        }else{
            $("#passok1").removeClass("text-black-50 text-danger")
            $("#passok1").addClass("text-success")
            $("#passok1").html("Password has at least 8 characters ")
            $("#passok1").nextUntil("p").remove()
            $("#passok1").append('<i class="fas fa-check"></i>')
        }
        if(/\s/.test($("#pw1").val())){
            $("#passok2").removeClass("text-black-50 text-success")
            $("#passok2").addClass("text-danger")
            $("#passok2").html("Password has one or more spaces ")
            $("#passok2").nextUntil("p").remove()
            $("#passok2").append('<i class="fas fa-times"></i>')
        }else{
            $("#passok2").removeClass("text-black-50 text-danger")
            $("#passok2").addClass("text-success")
            $("#passok2").html("Password does not contain spaces ")
            $("#passok2").nextUntil("p").remove()
            $("#passok2").append('<i class="fas fa-check"></i>')
        }
        if($("#pw1").val() !== $("#pw2").val()){
            $("#passok3").removeClass("text-black-50 text-success")
            $("#passok3").addClass("text-danger")
            $("#passok3").html("Passwords do not match ")
            $("#passok3").nextUntil("p").remove()
            $("#passok3").append('<i class="fas fa-times"></i>')
        }else{
            $("#passok3").removeClass("text-black-50 text-danger")
            $("#passok3").addClass("text-success")
            $("#passok3").html("Passwords match ")
            $("#passok3").nextUntil("p").remove()
            $("#passok3").append('<i class="fas fa-check"></i>')
        }
    }
})

$("#register").on("click", function(e){
    if($("#emailok").hasClass("text-danger") || $("#passok1").hasClass("text-danger") || $("#passok2").hasClass("text-danger") || $("#passok3").hasClass("text-danger")){
        $(".modal-content").addClass("animated shake")
        setTimeout(function(){
            $(".modal-content").removeClass("animated shake")
        }, 500)
        e.preventDefault()
    }
})